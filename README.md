# Visualiseur des données des ressources d'Open Data France

Widget **[Datami](https://gitlab.com/multi-coop/datami)** de prévisualisation des données issues de fichiers csv des ressourcces - Open Data France.

---

## Résumé

Open Data France possède un ensemble de ressources à visualiser et à partager sous la forme d'un widget...

Afin de pouvoir partager et de mettre en valeur toutes ces ressources il a été proposé d'utiliser le _widget open source_ créé par la coopérative numérique **[multi](https://multi.coop)** : le projet **[Datami](https://gitlab.com/multi-coop/datami)**.

Cet outil permet la **visualisation** des données mais également la **contribution**.

**Datami** permet d'intégrer sur des sites tiers (sites de partenaires ou autres) une sélection plus ou moins large de ressources. Cette solution permet à la fois d'éviter aux sites partenaires de "copier-coller" les ressources, d'afficher sur ces sites tiers les ressources toujours à jour, et de permettre aux sites tiers ainsi qu'au site source de gagner en visibilité, en légitimité et en qualité d'information.

L'autre avantage de cette solution est qu'elle n'est déployée qu'une fois, mais que le widget peut être intégré et paramétré/personnalisé sur autant de sites tiers que l'on souhaite... **gratuitement**.

---

## Démo

- Page html de démo : [![Netlify Status](https://api.netlify.com/api/v1/badges/ef33715e-c076-40e0-beaa-7479afe8a7d1/deploy-status)](https://app.netlify.com/sites/datami-demo-odf-data-ressources/deploys)
- URL de démo :
  - DEMO / données des ressources d'ODF : https://datami-demo-odf-data-ressources.netlify.app/

---

### Documentation 

Un site dédié à la documentation technique de Datami est consultable ici : https://datami-docs.multi.coop

---

## Crédits

| | logo | url |
| :-: | :-: | :-: |
| **Open Data France** | ![ODF](./images/odf-logo.svg) | https://www.opendatafrance.net/ |
| **coopérative numérique multi** | ![multi](./images/multi-logo.png) | https://multi.coop |

---

## Pour aller plus loin... 

### Datami

Le widget fait partie intégrante du projet [Datami](https://gitlab.com/multi-coop/datami)
